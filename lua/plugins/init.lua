return {
  {
    "stevearc/conform.nvim",
    -- event = 'BufWritePre', -- uncomment for format on save
    config = function()
      require "configs.conform"
    end,
  },
  {
    "neovim/nvim-lspconfig",
     config = function()
       require("nvchad.configs.lspconfig").defaults()
       require "configs.lspconfig"
     end,
  },
  {
    "nvimtools/none-ls.nvim",
    ft = {"python"},
    opts = function()
      require "configs.null-ls"
    end,
  },
  {
    "williamboman/mason.nvim",
  	opts = {
  		ensure_installed = {
        "pyright", "ruff", "mypy"
  			-- "lua-language-server", "stylua",
  			-- "html-lsp", "css-lsp" , "prettier"
  		},
  	},
  },
--  {
--  	"nvim-treesitter/nvim-treesitter",
--  	opts = {
--  		ensure_installed = {
--  			"vim", "lua", "vimdoc",
--       "html", "css"
--  		},
--  	},
--  },
}
