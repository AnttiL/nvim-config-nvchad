local null_ls = require("null-ls")

local options = {
  sources = {
    null_ls.builtins.diagnostics.mypy,
    null_ls.builtins.diagnostics.ruff,
  },
  debug = true,
}

null_ls.setup {
  debug = true,
}

return options

